#include <fcntl.h>
#include <getopt.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/in.h>

#include "fuzzles.h"

/*
 * Fuzzes SNMP, with a view to:
 *  - tracking responses, to spot inconsistencies
 *  - being _fast_
 *
 * Constructs individual SNMP queries to servers, then retries the same queries
 * as multi-requests in random order. Output to file for parallel parsing.
 *
 * In-memory copies in the transmission loop are avoided where possible.
 */

/*
 * Returns a new transmit fd connected to the target
 */
int txconnect(const char *target, int port) {
  int fd, ret;
  struct sockaddr_in saddr;
  struct in_addr sinaddr;

  inet_aton(target, &sinaddr);

  saddr.sin_family = AF_INET;
  saddr.sin_port = htons(port);
  saddr.sin_addr = sinaddr;

  fd = socket(AF_INET, SOCK_DGRAM, 0);
  ret = connect(fd, (struct sockaddr *)&saddr, sizeof(struct sockaddr_in));

  if ((ret < 0) || (fd < 0)) {
    perror("Couldn't open socket to target");
    return -1;
  }

  return fd;
}

/*
 * Encodes an OID string of the form 1.2.3.6533.4.3 into the provided buffer.
 *
 * Returns the number of bytes consumed.
 */
int encode_oid(const char *oid, unsigned char *buf) {
  unsigned char *buf_start = buf;
  int value = 0, bytes;

  if (*oid == '.')
    oid++;

  // note we assume the first two digits are one character only FIXME
  *buf = (*oid - 0x30) * 40;
  oid += 2;
  *buf += (*oid - 0x30);
  oid += 2;
  buf++;

  while (1) {
    if ((*oid == '.') || (*oid == '\0')) {
      // FIXME improve this with Maths
      for (bytes=0; ; bytes++) {
        if ((value >> (7*bytes)) == 0) break;
      }

      buf+=bytes;

      while (value > 0) {
        buf--;
        *buf = (value & 0xff) | 0x80; // high bit indicates partial value; remaining 7 bits encode data
        value >>= 7;
      }

      buf += bytes;
      *(buf-1) &= ~0x80; // clear the high bit on the last byte

      value = 0;

      if (*oid == '\0') break;
    } else {
      // collect input digits
      value *= 10;
      value += (*oid - 0x30);
    }

    oid++;
  }

  return buf - buf_start;

}

/*
 * Adds a new TLV to the buffer at idx. Returns the new TLV
 */
snmp_tlv *add_tlv(unsigned char *idx, int type, int len, const unsigned char *data) {
  snmp_tlv *tlv = (snmp_tlv *)idx;

  tlv->type = type;
  tlv->len = len;
  if (len > 0)
    memcpy(TLV_DATA(tlv), data, len);

  return tlv;
}

/*
 * Adds a TLV as subordinate to an existing TLV. Any existing data in the
 * buffer is written over.
 */
snmp_tlv *add_sub_tlv(snmp_tlv *parent, int type, int len, const unsigned char *data) {
  return add_tlv(TLV_DATA(parent), type, len, data);
}

snmp_tlv *add_adj_tlv(snmp_tlv *sibling, int type, int len, const unsigned char *data) {
  // clean up this cast if this becomes the main use of TLV_ADJ
  return add_tlv((unsigned char *)TLV_ADJ(sibling), type, len, data);
}

/*
 * Adds a TLV header only as a subordinate to an existing TLV.
 */
snmp_tlv *add_sub_tlv_header(snmp_tlv *parent, int type) {
  return add_tlv(TLV_DATA(parent), type, 0, NULL);
}

snmp_tlv *add_adj_tlv_header(snmp_tlv *sibling, int type) {
  return add_tlv((unsigned char *)TLV_ADJ(sibling), type, 0, NULL);
}

/*
 * Prepares the next packet for transmission.
 *
 * buf: This will be filled with the new packet, up to BUFLEN
 * plen: set to the size of the new packet
 * bitmask: will be filled with the packet bitmask for random permuting
 * community: A string specifying the SNMP v1 community to use in packets to the target
 */
int build_sample_packet(unsigned char *buf, unsigned int *plen, unsigned char *bitmask, const char *community_s) {

  // initialise packet
  snmp_tlv *message = (snmp_tlv *)buf;
  message->type = SNMP_SEQUENCE;

    // Message/Version
    snmp_tlv *version = add_sub_tlv(message, SNMP_INTEGER, 1, (const unsigned char *)"\0");

    // Message/Community
    snmp_tlv *community = add_adj_tlv(version, SNMP_STRING, strlen(community_s), (const unsigned char *)community_s);

    // Message/PDU
    snmp_tlv *pdu = add_adj_tlv_header(community, SNMP_GET_REQUEST);

      // PDU/Request ID
      snmp_tlv *req_id = add_sub_tlv(pdu, SNMP_INTEGER, 2, (const unsigned char *)"\0\0");

      if (bitmask)
        BITMASK_SET_RANGE(TLV_DATA(req_id), 2); // should always be randomised, so we can tell requests apart for postmortem

      // PDU/Error
      snmp_tlv *req_error = add_adj_tlv(req_id, SNMP_INTEGER, 1, (const unsigned char *)"\0");

      // PDU/Error index
      snmp_tlv *req_error_index = add_adj_tlv(req_error, SNMP_INTEGER, 1, (const unsigned char *)"\0");

      // PDU/Varbind list
      snmp_tlv *varbind_list = add_adj_tlv_header(req_error_index, SNMP_SEQUENCE);

        // PDU/Varbind list/Varbind
        snmp_tlv *varbind = add_sub_tlv_header(varbind_list, SNMP_SEQUENCE);

          // PDU/Varbind list/Varbind/OID
          snmp_tlv *oid = add_sub_tlv_header(varbind, SNMP_OID);
          oid->len = encode_oid("1.3.6.1.4.1.35156.17.1.1", TLV_DATA(oid));
          if (bitmask)
            BITMASK_SET_RANGE(&(oid->len), TLV_LEN(oid)-1);

          // PDU/Varbind list/Varbind/Value
          snmp_tlv *oid_value = add_adj_tlv_header(oid, SNMP_NULL);
          unsigned char *message_end = (unsigned char *)TLV_ADJ(oid_value);

        varbind->len = message_end - TLV_DATA(varbind);

      varbind_list->len = message_end - TLV_DATA(varbind_list);

    pdu->len = message_end - TLV_DATA(pdu);

  message->len = message_end - TLV_DATA(message);

  *plen = TLV_LEN(message);

  return 1;
}

/*
 * Prepares the next packet for transmission, using a packet definition
 *
 * A series of random bytes determines the packet structure.
 * Each byte corresponds to one TLV
 * MSB - whether adjacent or subordinate?
 * LS 3 bits - packet type
 *
 * buf: This will be filled with the new packet, up to BUFLEN
 * plen: set to the size of the new packet
 * bitmask: will be filled with the packet bitmask for random permuting
 * community: A string with the SNMP v1 community to use in packets to the target
 * def: the packet definition
 * def_s: packet definition length
 */
int build_packet(unsigned char *buf, unsigned int *plen, unsigned char *bitmask, const char *community_s, unsigned char *def, int def_s) {

  int use_bitmask = 0;

  // initialise packet
  snmp_tlv *message = (snmp_tlv *)buf;
  message->type = SNMP_SEQUENCE;

    // Message/Version
    snmp_tlv *version = add_sub_tlv(message, SNMP_INTEGER, 1, (const unsigned char *)"\0");

    // Message/Community
    snmp_tlv *community = add_adj_tlv(version, SNMP_STRING, strlen(community_s), (const unsigned char *)community_s);

    // Message/PDU
    snmp_tlv *pdu = add_adj_tlv_header(community, SNMP_GET_REQUEST);

      // Build the rest from the definition
      snmp_tlv *stack[STRUCTURE_STACK_DEPTH];
      snmp_tlv **parent = stack;

      snmp_tlv *head = (snmp_tlv *)TLV_DATA(pdu);
      unsigned char *def_p = def;

      for (; def_p < def+def_s; def_p++) {
        int type;

        // Decide what type of TLV: bottom three bits
        switch (*def_p & 0x07) {
          case 0x00:
            type = SNMP_OID;
            break;
          case 0x01:
            type = SNMP_GET_REQUEST;
            break;
          case 0x02:
            type = SNMP_INTEGER;
            break;
          //case 0x03:
          //  type = SNMP_STRING;
          //  break;
          //case 0x04:
          //  type = SNMP_NULL;
          //  break;
          default:
            type = SNMP_SEQUENCE;
            break;
        }

        int len = (*def_p & 0x38) >> 3; // length 0-8 (centre three bits)

        head->type = type;
        head->len = len;

        // Decide whether to recurse deeper, pop up, or stay adjacent
        switch ((*def_p & 0xC0) >> 6) {
          case 0x00:
            // adjacent - set bitmask to ensure the payload is randomised
            LOG_DEBUG("%p: Creating adjacent\n", head);
            BITMASK_SET_RANGE(TLV_DATA(head), len);
            use_bitmask = 1;
            head = (snmp_tlv *)TLV_ADJ(head);
            break;
          case 0x02:
            // set the parent's length and pop up
            if (parent > stack) {
              (*parent)->len = (head + head->len) - (snmp_tlv *)TLV_DATA(*parent);
              LOG_DEBUG("%p: Closing parent %p and creating new subordinate of granparent %p\n", head, *parent, *(--parent));
            } else {
              LOG_DEBUG("%p: Creating adjacent (stack empty, would pop)\n", head);
            }
            head = (snmp_tlv *)TLV_ADJ(head);
            break;
          default:
            // subordinate
            if (parent < stack + 8192 - sizeof(parent)) {
              LOG_DEBUG("%p: Creating subordinate\n", head);
              parent++;
              *parent = head;
              head = (snmp_tlv *)TLV_DATA(head);
            } else {
              LOG_DEBUG("%p: Creating adjacent (stack full, would push)", head);
              head = (snmp_tlv *)TLV_ADJ(head);
            }
            break;
        }
      }

      head--;
      unsigned char *message_end = (unsigned char *)TLV_ADJ(head);

      while (parent > stack) {
        (*parent)->len = message_end - TLV_DATA(*parent);
        LOG_DEBUG("%p: Setting length of this parent to %d\n", *parent, (*parent)->len);
        parent--;
      }

    pdu->len = message_end - TLV_DATA(pdu);

  message->len = message_end - TLV_DATA(message);

  *plen = TLV_LEN(message);

  return use_bitmask;
}

char *hex(unsigned char *buf, int len) {
  char *ret = malloc(len*2+1);

  char *ptr = ret;
  for (; len>0; --len) {
    snprintf(ptr, 3, "%02x", *(buf++));
    ptr += 2;
  }

  return ret;
}

/*
 * Log packet in buf of length plen to fd.
 *
 * Note plen must always be the same as specified in log_packet_init (which
 * must be called once to create output file headers)
 *
 * Packets are logged to output in a binary format as below:
 * ...[time elapsed in seconds, 2 bytes][packet data, plen bytes]...
 *
 * Timestamps and other numbers are stored on disk in network byte order,
 * because I'm a terrible person.
 *
 * Timestamp is a (rough) seconds-since-the-epoch until I have a better idea
 * that works quickly.
 */
void log_packet(FILE *f, uint16_t timestamp, unsigned char *buf, uint16_t plen) {
  timestamp = htons(timestamp);
  fwrite(&timestamp, sizeof(timestamp), 1, f);
  fwrite(buf, 1, plen, f);
}

/*
 * Sets up the binary log header. This is in the format:
 *
 * [version, 1 byte][plen, 2 bytes][start timestamp, 4 bytes]
 *
 * ...I plan for it to get more complicated? :P
 * Future ideas; compress this further by logging the full packet and bitmask
 * once in the header, then only write fuzzed bytes.
 *
 * Timestamps and other numbers are stored on disk in network byte order,
 * because I'm a terrible person.
 *
 * Returns a new file descriptor for the output file.
 */
FILE *log_packet_init(const char *filename, uint16_t plen, uint32_t timestamp) {
  FILE *f;
  if (!(f = fopen(filename, "w+"))) {
    perror("Couldn't open logfile");
  }

  int version = LOG_VERSION;
  if (!fwrite(&version, 1, 1, f)) {
    // check that the first write succeeded; don't bother checking later calls
    perror("Error writing to log");
    return NULL;
  }

  plen = htons(plen);
  fwrite(&plen, sizeof(plen), 1, f);

  LOG_DEBUG("Start timestamp = %d\n", timestamp);
  timestamp = htonl(timestamp);
  fwrite(&timestamp, sizeof(timestamp), 1, f);

  return f;
}

/*
 * Create a slowly-expanding sequence of alternatively-inverted bytes
 * Deterministic, always sends the same packets each run
 * Speed: "fairly quick"
 *
 * Two state variables:
 *   - s_workmask: should be intiialised to a copy of the bitmask, then
 *     preserved between calls
 *   - s_bookmark: initialised to zero and preserved between calls
 *
 * All these bitmask_permute_ functions conform to the same interface. They all
 * return zero when there's no more permutations, one otherwise.
 */
int bitmask_permute_alternate(unsigned char *buf, int len, unsigned char *bitmask, unsigned char *s_workmask, int *s_bookmark) {
  while(bitmask[*s_bookmark] == 0) {
    (*s_bookmark)++;
    if (*s_bookmark > len) return 0;
  }

  // bookmark points to the highest byte we're working on. find the first set
  // bit in the workmask, clear it, and flip the corresponding bit in the
  // buffer
  int i, f=0;
  for (i=0; i<=*s_bookmark; i++) {
    if (s_workmask[i] == 0) continue;
    f = ffs(s_workmask[i]);
    break;
  }

  if (f > 0) {
    f--;
    s_workmask[i] ^= (1<<f);
    buf[i] ^= (1<<f);
    //LOG_DEBUG("flipping bit %d, bookmark = %d, i=%d, s_workmask[i] = %x, buf[i] = %x\n", f, *s_bookmark, i, s_workmask[i], buf[i]);
  } else {
    // run out of bits, find us a new bookmark
    memcpy(s_workmask, bitmask, BUFLEN);
    (*s_bookmark)++;
  }

  return 1;
}

/*
 * Hey guys I made the permute function O(n)! Do I win a prize?
 * Fast, but only 5000/time
 *
 * This guy stops at a certain number of permutations to avoid a segfault :P
 */
int main();
int bitmask_permute_wtf(unsigned char *buf, int len, unsigned char *bitmask, unsigned char *a, int *s_counter) {
  int i;
  for (i=0; i<len; i++) {
    if (bitmask[i] == 0) continue;
    buf[i] ^= (*((unsigned char *)&main + i + *s_counter) & bitmask[i]);
  }

  if ((*s_counter)++ > 5000) { // much more and things get sad
    return 0;
  } else {
    return 1;
  }

}

/*
 * As above but with urandom (v slow)
 */
int bitmask_permute_urandom(unsigned char *buf, int len, unsigned char *bitmask, unsigned char *a, int *fp) {
  FILE *random;
  if (*fp == 0) {
    random = fopen("/dev/urandom", "r"); // leaks a fd, bleh
    *fp = fileno(random);
  } else {
    random = fdopen(*fp, "r");
  }

  int i;
  for (i=0; i<len; i++) {
    if (bitmask[i] == 0) continue;
    buf[i] ^= ((unsigned char)fgetc(random) & bitmask[i]);
  }

  return 1;
}

/*
 * Seed all the random things we might use
 */
void seed_random() {
  FILE *random;
  unsigned int seed;

  random = fopen("/dev/urandom", "r");

  fgets((char *)&seed, sizeof(seed), random);
  srand(seed);

  fgets((char *)&seed, sizeof(seed), random);
  srandom(seed);

  fclose(random);
}


/*
 * As above but with rand (25,000/sec)
 */
int bitmask_permute_rand(unsigned char *buf, int len, unsigned char *bitmask, unsigned char *a, int *b) {
  int i;
  for (i=0; i<len; i++) {
    if (bitmask[i] == 0) continue;
    buf[i] ^= ((unsigned char)rand() & bitmask[i]);
  }

  return 1;
}

/*
 * As above but with random (better rand) (34,000/sec)
 */
int bitmask_permute_random(unsigned char *buf, int len, unsigned char *bitmask, unsigned char *a, int *b)  {
  int i;
  for (i=0; i<len; i++) {
    if (bitmask[i] == 0) continue;
    buf[i] ^= ((unsigned char)random() & bitmask[i]);
  }

  return 1;
}

void hexdump(unsigned char *buf, int len) {
  for (; len>0; len--) {
    printf("%02x", *(buf++));
  }

  printf("\n");
}

// probes a target with a get request to see if it's still alive
// timeout is in milliseconds
// returns 0 if the target is responsive; 1 otherwise
int probe(const char *target, const int port, const char *community, int timeout) {
  unsigned char buf[BUFLEN];
  unsigned int plen;
  int fd, ret;

  memset(buf, 0, BUFLEN);
  build_sample_packet(buf, &plen, NULL, community);

  // connect socket
  if ((fd = txconnect(target, port)) == -1) {
    // perror already prints an error message
    return 1;
  }

  send(fd, buf, plen, MSG_DONTWAIT); // TODO - make this block? perhaps

  struct pollfd rx_fds = { fd, POLLIN, 1  };
  ret = poll(&rx_fds, 1, timeout);

  if (ret <= 0) {
    // timed out
    LOG_ERROR("failed - target is unresponsive\n");
    goto fail;
  }

  ret = read(fd, buf, BUFLEN);
  if (ret < 0) {
    perror("couldn't read response from target");
    goto fail;
  }

  LOG_INFO("got %d bytes back from target: ", ret);
  hexdump(buf, ret > BUFLEN ? BUFLEN : ret);
  LOG_INFO("\n");
  close(fd);
  return 0;

fail:
  close(fd);
  return 1;
}

int signal_flag;
void catch_sig(int signo) {
  signal_flag = 1;
}

void usage() {
  printf("fuzzles: a high-speed SNMP fuzzer.\n");
  printf("\n");
  printf("  -t, --target [IP]                 IP of the target (%s).\n", TARGET);
  printf("  -p, --port [PORT]                 Port to direct packets to (%d).\n", TPORT);
  printf("  -c, --community [COMMUNITY]       SNMP v1 community to use in packets to the target (%s).\n", COMMUNITY);
  printf("  -s, --static                      Use a static sample packet, rather than generating a structure randomly every\n");
  printf("                                    n permutations. Implies --permutations=infinity (false)\n");
  printf("  -x, --structures [STRUCTURES]     The number of random TLVs to generate in each packet (%d).\n", STRUCTURES);
  printf("  -n, --permutations [PERMUTATIONS] The number of packet permutations to try for each generated structure (%d).\n", PERMUTATIONS);
  printf("  -f, --logfile [PATH]              Log all packets sent to this file in a binary format. This can get big! (no logging).\n");
  printf("  -v, --verbose [LEVEL]             Logging verbosity, from 0 to 2 (%d).\n", log_level);
  printf("  -h, --help                        This message\n");
  printf("\n");

}

int main(int argc, char **argv) {
  unsigned int txfd, ret, plen, elapsed, timestamp;
  unsigned long packetcounter = 0;
  int state = 0;
  struct timeval start_t;
  unsigned char buf[BUFLEN], bitmask[BUFLEN], workmask[BUFLEN]; // main buffer
  void **state_blob;

  // option processing
  char *target = TARGET;
  char *community = TARGET;
  int port = TPORT;
  int static_structure = 0;
  int n_structures = STRUCTURES;
  int permutations = PERMUTATIONS;
  char *logfile = NULL;
  // log_level defined in fuzzles.h

  static struct option longopts[] = {
    {"target", required_argument, NULL, 't'},
    {"port", required_argument, NULL, 'p'},
    {"community", required_argument, NULL, 'c'},
    {"static", no_argument, NULL, 's'},
    {"structures", required_argument, NULL, 'x'},
    {"permutations", required_argument, NULL, 'n'},
    {"logfile", required_argument, NULL, 'f'},
    {"verbose", required_argument, NULL, 'v'},
    {"help", no_argument, NULL, 'h'},
  };

  char ch;
  while ((ch = getopt_long(argc, argv, "t:p:c:sx:n:f:v:h", longopts, NULL)) != -1) {
    switch (ch) {
      case 't':
        if ((strlen(optarg) == 0) || (strlen(optarg) > 15)) {
          LOG_ERROR("Error: --target requires a valid IP argument\n");
          return 1;
        }
        target = strdup(optarg);
        break;
      case 'p':
        port = atoi(optarg);
        if ((port <= 0) || (port > 65536)) {
          LOG_ERROR("Error: --port requires a valid UDP port\n");
          return 1;
        }
        break;
      case 'c':
        if (strlen(optarg) == 0) {
          LOG_ERROR("Error: --community requires a valid SNMP v1 community string\n");
          return 1;
        }
        community = strdup(optarg);
        break;
      case 's':
        static_structure = 1;
        break;
      case 'x':
        n_structures = atoi(optarg);
        if (n_structures <= 0) {
          LOG_ERROR("Error: --structures requires a numeric argument\n");
          return 1;
        }
        break;
      case 'n':
        permutations = atoi(optarg);
        if (permutations <= 0) {
          LOG_ERROR("Error: --permutations requires a numeric argument\n");
          return 1;
        }
        break;
      case 'f':
        if (strlen(optarg) == 0) {
          LOG_ERROR("Error: --logfile requires a valid argument\n");
          return 1;
        }
        logfile = strdup(optarg);
        break;
      case 'v':
        log_level = atoi(optarg);
        if ((log_level <= 0) || (log_level > 2)) {
          LOG_ERROR("Error: --verbose requires a numeric argument between 0 and 2\n");
          return 1;
        }
        break;
      case 'h':
      default:
        usage();
        return 0;
    }
  }

  LOG_INFO("Initialising - target %s:%d\n%d permutations per structure, %d TLVs\n\n", target, port, permutations, n_structures);

  if (static_structure) {
    LOG_INFO("Not permuting packet structure\n");
  }

  // signals
  signal_flag = 0;
  signal(SIGINT, catch_sig);
  signal(SIGHUP, catch_sig);

  // connect socket
  if ((txfd = txconnect(target, port)) == -1) {
    // perror already prints an error message
    return 1;
  }

  // init random
  seed_random();

  struct pollfd tx_fds = { txfd, POLLOUT, 0 };

  FILE *log_fd;
  if (logfile) {
    // create binary output file
    log_fd = log_packet_init(logfile, plen, (uint32_t)(start_t.tv_sec));
    LOG_INFO("Logging packets sent to %s\n", logfile);
  }

  unsigned char *def = malloc(n_structures);
  while (!signal_flag) {
    memset(buf, 0, BUFLEN);
    memset(bitmask, 0, BUFLEN);
    memset(workmask, 0, BUFLEN);

    // prepare the packet definition and packet
    for (int i=0; i<n_structures; i++) {
      def[i] = (unsigned char) random();
    }

    //LOG_INFO("Packet definition: ");
    //hexdump(def, 50);
    //LOG_INFO("\n");

    // use_bitmask is an optimisation - if the bitmask is all zeros, don't
    // bother permuting it
    int use_bitmask;
    if (static_structure) {
      use_bitmask = build_sample_packet(buf, &plen, bitmask, community);
    } else {
      use_bitmask = build_packet(buf, &plen, bitmask, community, def, n_structures);
    }

    // prepare the bitmask
    memcpy(workmask, bitmask, BUFLEN);

    gettimeofday(&start_t, NULL);
    for(long i=0 ; (i < permutations || static_structure) && !signal_flag; i++) {
      ret = poll(&tx_fds, 1, 100);

      if (ret == 0) {
        // timed out
        LOG_INFO("\nTimed out (the output buffer is presumably full)\n");
        continue;
      }

      //hexdump(buf, plen);
      //LOG_INFO("\n");

      send(txfd, buf, plen, MSG_DONTWAIT);

      if (logfile) {
        log_packet(log_fd, (uint16_t)elapsed, buf, (uint16_t)plen); // timestamp just in seconds since the epoch for now
      }

      if ((i == 0) || (packetcounter % 1213) == 0 || (i+1 == permutations)) {
        struct timeval cur_t;
        long elapsed_s, elapsed_us;

        gettimeofday(&cur_t, NULL);
        elapsed_s = cur_t.tv_sec - start_t.tv_sec;
        elapsed_us = cur_t.tv_usec - start_t.tv_usec;

        if (elapsed_us < 0) {
          elapsed_s--;
          elapsed_us += 1000000;
        }

        int width = 50;
        char *packet = hex(buf, plen > width ? width : plen);
        char *dots = plen > width ? "..." : "";
        int pps = (elapsed_s ? i / elapsed_s : 0) + (i * 1000000 / elapsed_us);
        LOG_INFO("\r%ld sent in %ld.%ld seconds: %d pps \t\t%s%s%22s", i+1, elapsed_s, elapsed_us/1000, pps, packet, dots, "");

      }

      packetcounter++;

      if (!use_bitmask || !bitmask_permute_random(buf, plen, bitmask, workmask, &state)) {
        if (!static_structure)
          break;
      }

    }

    LOG_INFO("\n");
    LOG_DEBUG("\nCycle complete\n");

    int i;
    for (i=1; i<=PROBE_RETRIES; i++) {
      LOG_INFO("Probe %d/%d: ", i, PROBE_RETRIES);
      fflush(stdout);
      if (!(probe(target, port, COMMUNITY, PROBE_TIMEOUT)))
        break;
    }

    // Stop if we broke the target
    if (i > PROBE_RETRIES)
      break;

  }

  LOG_INFO("\n%lu permutations completed.\n", packetcounter);

  if (logfile) {
    fclose(log_fd);
  }

  return 0;
}
