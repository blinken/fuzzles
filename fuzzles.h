/* Various global parameters */
#define LOG_VERSION 1
#define BUFLEN 1500

// The maximum depth we'll nest structures when generating TLV structure
// randomly. Actual structure depth is dependent on the packet definition
// supplied to build_packet, this paramter just sets the size of the
// datastructure
#define STRUCTURE_STACK_DEPTH 8192

/* Option defaults */
#define TARGET "127.0.0.1"
#define TPORT 161
#define COMMUNITY "public"
#define STRUCTURES 50
#define PERMUTATIONS 20000
#define PROBE_TIMEOUT 500
#define PROBE_RETRIES 3

/* Logging macros */
int log_level = 1;

#define LOG_DEBUG if (log_level >= 2) printf
#define LOG_INFO  if (log_level >= 1) printf
#define LOG_ERROR if (log_level >= 0) printf

/* TLV structure processing */
typedef struct snmp_tlv_s {
  uint8_t type;
  uint8_t len;
} snmp_tlv;

#define SNMP_SEQUENCE 0x30
#define SNMP_GET_REQUEST 0xA0
#define SNMP_INTEGER 0x02
#define SNMP_STRING 0x04
#define SNMP_NULL 0x05
#define SNMP_OID 0x06

#define TLV_LEN(t) t->len + 2

// indexes the data section
#define TLV_DATA(t) ((unsigned char *)t+2)

// indexes the TLV adjacent
#define TLV_ADJ(t) (snmp_tlv *)(TLV_DATA(t) + t->len)

/* Packet definition shortcuts */
// Adjacent - MSB 2 bits = 11; otherwise subordinate (ie. 1/4 chance of adjacent)
#define IS_ADJ(x) (x & 0xC0)

/* Bitmask manipulation */
#define BITMASK_SET_BYTE(p) *(bitmask+((unsigned char *)p-buf)) = 0xFF
#define BITMASK_SET_RANGE(p, len) memset(bitmask+((unsigned char *)p-buf), 0xFF, len)
#define BITMASK_SET_RANGE_ALT(p, len) memset(bitmask+((unsigned char *)p-buf), 0x0F, len)

